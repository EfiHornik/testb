// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDT9CaWCCt3EVm18pTpUf7r441pdiwqg90",
    authDomain: "testb-5e805.firebaseapp.com",
    projectId: "testb-5e805",
    storageBucket: "testb-5e805.appspot.com",
    messagingSenderId: "1016524536605",
    appId: "1:1016524536605:web:04ea3169bf5c8ce2a33ff4"
  }
};



/*
 * **For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
