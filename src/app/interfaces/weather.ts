export interface Weather {
    name?:string,
    temperature:number,
    humidity:number,
    prediction?:string
}
