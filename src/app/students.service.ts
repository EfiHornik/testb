import { WeatherRaw } from './interfaces/weather-raw';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Weather } from './interfaces/weather';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

userCollection:AngularFirestoreCollection = this.db.collection('users');
studentsCollection:AngularFirestoreCollection;

getStudents(userId):Observable<any[]>{
  this.studentsCollection = this.db.collection(`users/${userId}/students`);
  return this.studentsCollection.snapshotChanges().pipe(
    map(
      collection =>collection.map(
        document=> {
          const data = document.payload.doc.data();
          data.id = document.payload.doc.id;
          return data; 
        }
      )  
   ))  
} 





addStudent(userId:string, temperature:number, humidity:number, prediction){
  const weather:Weather= {temperature:temperature,humidity:humidity, prediction:prediction}
  this.userCollection.doc(userId).collection('students').add(weather);
} 



constructor(private db: AngularFirestore,
  ) {} 

}