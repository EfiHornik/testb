import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';
import { Weather } from '../interfaces/weather';
import { PredictService } from '../predict.service';
import { StudentsService } from '../students.service';
import { WeatherService } from '../weather.service';

@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})

export class TemperaturesComponent implements OnInit {
  temperature:number; 
  image:string; 
  humidity:number; 
  weatherData$:Observable<Weather>;
  hasError:Boolean = false;
  errorMessage:string;
  show=true;
  showPred=true;
  showGetnew=true;
  prediction:string;
  students$;
  userId;
  city='London';


  
  getData(){
    this.weatherData$ = this.weatherService.searchWeatherData(this.city); 
    this.weatherData$.subscribe(
      data => {
        this.temperature = data.temperature;
        this.humidity = data.humidity;
      }, 
      error =>{
        console.log(error.message);
        this.hasError = true;
        this.errorMessage = error.message; 
      }
    )
  }

predict(temperature,humidity){
  console.log(temperature,humidity);
  this.predictService.predict(temperature,humidity).subscribe(
    res => {
      console.log(res);
    if(res>50){
      this.prediction='Will rain'
    }else{
      this.prediction='Will not rain'
    }console.log(this.prediction);
    return this.prediction;
   }
    )

    }
  constructor(private route:ActivatedRoute, private weatherService:WeatherService,public predictService:PredictService, public authService:AuthService,private studentsService:StudentsService) { }

  save(temperature,humidity,prediction){
    this.studentsService.addStudent(this.userId,this.temperature,this.humidity, this.prediction);
  }
  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
       
        
    })}

  }