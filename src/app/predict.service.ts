import { TemperaturesComponent } from './temperatures/temperatures.component';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictService {
  url = 'https://lhtkr5o75e.execute-api.us-east-1.amazonaws.com/efitest'; 
  
  predict(temperature:number, humidity:number):Observable<any>{
    let json = 
        {
          "temp": temperature,
          "humidity": humidity,
 
        }
    
    let body  = JSON.stringify(json);
    console.log("in predict");
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log("in the map"); 
        console.log(res);
        console.log(res.body);
        return res;       
      })
    );      
  }

  
  constructor(private http: HttpClient) { }
}
