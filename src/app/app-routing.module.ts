import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { SavedComponent } from './saved/saved.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { TemperaturesComponent } from './temperatures/temperatures.component';
import { WelcomeComponent } from './welcome/welcome.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'sign-up', component: SignUpComponent },
  { path: 'welcome', component: WelcomeComponent },
  { path: 'temperatures', component: TemperaturesComponent },
  { path: 'saved', component:SavedComponent }


  

  
  
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
