import { Component,OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { StudentsService } from '../students.service';
import { Weather } from '../interfaces/weather';
import { PredictService } from '../predict.service';

@Component({
  selector: 'saved',
  templateUrl: './saved.component.html',
  styleUrls: ['./saved.component.css']
})
export class SavedComponent implements OnInit {

  students:Weather[];
  students$;
  userId:string;
  addCustomerFormOpen= false;

  temperature:number;
  humidity:number;
  
id:string;

  prediction='unknown';

  constructor(private studentsService:StudentsService, public authService:AuthService,private predictService:PredictService ) { }
  
  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.students$ = this.studentsService.getStudents(this.userId);
        
    })}

}

